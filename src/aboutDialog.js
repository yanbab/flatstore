/* aboutDialog.js
 *
 * Copyright 2021 Yan Babilliot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public Lice   nse
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Gtk, GObject } = imports.gi;


var AboutDialog = GObject.registerClass({
  GTypeName: 'AboutDialog',
}, class AboutDialog extends Gtk.AboutDialog {

  _init(params) {
    super._init({
      //artists: [],
      authors: ['Yan Babilliot <yanbab@gmail.com>'],
      //translator_credits: "",
      program_name: "FlatStore",
      comments: "Install, run and remove applications",
      license_type: Gtk.License.GPL_3_0,
      logo_icon_name: 'com.gitlab.yanbab.flatstore',
      version: '0.1',
      website: 'https://gitlab.com/yanbab/flatstore',
      copyright: 'Copyright ©2021 Yan Babilliot',
      wrap_license: true,
      modal: true,
      transient_for: this.window,
      ...params
    });
  }

});

