/* window.js
 *
 * Copyright 2021 Yan Babilliot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Adw, Gio } = imports.gi;
const { AboutDialog } = imports.aboutDialog;

function fixAdwaita() {
  // Workaround missing builder declaration file
  // by first creating instances of problematic widgets
  new Adw.HeaderBar();
}

var FlatstoreWindow = GObject.registerClass({
    GTypeName: 'FlatstoreWindow',
    Template: 'resource:///com/gitlab/yanbab/flatstore/window.ui',
    InternalChildren: ['label']
}, class FlatstoreWindow extends Gtk.ApplicationWindow {

    _init(application) {
      fixAdwaita();
      super._init({ application });
      this._action('about', () => new AboutDialog({ transient_for: this }).show(),"<primary>a");
    }

  _action(name, handler, accel) {
    let action = new Gio.SimpleAction({ name });
    action.connect('activate', handler.bind(this.application));
    this.application.add_action(action);
    if(accel) this.application.set_accels_for_action( 'app.' + name,[ accel ]);
  }

});

